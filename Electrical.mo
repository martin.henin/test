model Electrical
  Modelica.Electrical.Analog.Basic.Resistor resistor1(R = 1)  annotation(
    Placement(visible = true, transformation(origin = {4, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage1 annotation(
    Placement(visible = true, transformation(origin = {-80, -4}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Basic.Inductor inductor1(L = 1)  annotation(
    Placement(visible = true, transformation(origin = {-40, 42}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = 1)  annotation(
    Placement(visible = true, transformation(origin = {66, 0}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {-80, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(constantVoltage1.p, inductor1.p) annotation(
    Line(points = {{-80, 6}, {-80, 42}, {-50, 42}}, color = {0, 0, 255}));
  connect(resistor1.p, inductor1.n) annotation(
    Line(points = {{4, 10}, {4, 42}, {-30, 42}}, color = {0, 0, 255}));
  connect(capacitor1.p, inductor1.n) annotation(
    Line(points = {{66, 10}, {66, 42}, {-30, 42}}, color = {0, 0, 255}));
  connect(ground1.p, constantVoltage1.n) annotation(
    Line(points = {{-80, -56}, {-80, -14}}, color = {0, 0, 255}));
  connect(constantVoltage1.n, resistor1.n) annotation(
    Line(points = {{-80, -14}, {-80, -34}, {4, -34}, {4, -10}}, color = {0, 0, 255}));
  connect(resistor1.n, capacitor1.n) annotation(
    Line(points = {{4, -10}, {4, -34}, {66, -34}, {66, -10}}, color = {0, 0, 255}));

annotation(
    uses(Modelica(version = "3.2.3")));
end Electrical;
